import { useEffect } from "react";
import { motion, useAnimation} from "framer-motion"
import styles from "./../styles/Home.module.css"

const animationVariants = {
    visible: { opacity: 1 },
    hidden: { opacity: 0 },
}

const Event = props => {

    const animationControls = useAnimation();
    const {event} = props
    useEffect(() => {
           
                animationControls.start("visible");
          
        }
    );

    const styling = {
        backgroundImage: `url('${event.images}')`,
        minWidth:"40vh",
        minHeight:"50vh",
        backgroundRepeat:"no-repeat",
        backgroundSize:"cover",
        marginTop:"5%"
    }


    return(
        <motion.div
            initial={"hidden"}
            animate={animationControls}
            variants={animationVariants}
            transition={{ ease: "easeInOut", duration: 3     }}
            style={styling}
            className={styles.imageEase}
        >
            <h3>{event.title}</h3>
            <p>{event.description}</p>
            <small>{event.date}</small>
        </motion.div>
    );
}

export default Event