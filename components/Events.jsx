import { events } from "../utils/data";
import Event from "./Event";
import styles from "./../styles/Home.module.css"

const Events = ()=>{

    return(
        <div className={styles.main}>
            {
                events.map(eachEvent => <Event event={eachEvent} key={eachEvent.id}/>)
            }
        </div>
    )
}

export default Events